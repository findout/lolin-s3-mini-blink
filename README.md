# Blink for LOLIN S3 MINI

I had some problems getting this board up and running, so here is what worked for me - at last.
I use PlatformIO inside VS Code.

- Checkout this project on the command line with:

      git clone https://gitlab.com/findout/lolin-s3-mini-blink.git

- Install PatformIO, by going to the extensions tab and search for PlatformIO and click install
- A new tab for PlatformIO is added - an alien face
- Click menu File > Open Folder... and select the folder lolin-s3-mini-blink
- Click the build button (a checkmark), on the bottom status bar, it should log a lot and end in SUCCESS
  - If you get an error message about platform or board not found, you may have to update the platform support files, by clicking the PlatformIO tab (alien face) and then Platforms under QUICK ACCESS > PIO Home, Then click Tab Updates and the update button by Espessif 32. If you don't see Espressif 32, you already have the latest platform files.
- Connect your board with a USB cable
- Click the upload button (a right arow) and wait for the upload loggings to end in SUCCESS
- the LED on the board should change color every second

The first time you upload to the board, you may have to get it into boot mode by holdng the boot button (marked 47 O), then press and release the RST button and then release the boot button. I had to do this just after I clicked the upload button in PlatformIO. I also had to get a good USB cable to get it working. This may be overcome by setting a lower upload speed in platformio.ini (google for platformio upload_speed to see how).

I use Linux and I also had to install a file that makes the board USB work properly. Follow the instruction in https://docs.platformio.org/en/stable/core/installation/udev-rules.html.

Good Luck!

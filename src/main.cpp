#include <Arduino.h>

#include <stdio.h>
#include "Arduino.h"
#include <Adafruit_NeoPixel.h>

#define NUM_LEDS 1  // Change this to the number of LEDs in your strip
#define DATA_PIN 47  // Change this to the GPIO pin to which the data line of your LED strip is connected

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, DATA_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(115200);
  Serial.println("1Hello, ESP32-S3!");
  strip.begin();
  strip.show();  // Initialize all pixels to 'off'
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
    strip.show();
    delay(wait);
  }
}

void loop() {

  Serial.println("Hello, ESP32-S3!");
  // Fill the entire strip with a red color
  colorWipe(strip.Color(255, 0, 0), 1000);
  delay(1000);
 // Fill the entire strip with a green color
  colorWipe(strip.Color(0, 255, 0), 1000);
  delay(1000);
}